<?php

namespace App\Constants;

class AuthConstantType
{
    const DEACTIVE = 0;
    const ACTIVE = 1;
    const DELETED = 2;
}