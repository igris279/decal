<?php

namespace App\Repositories\UserEloquent;

interface UserRepositoryInterface
{
    /**
     * @param @name
     */
    public function hasPermission($name);
}