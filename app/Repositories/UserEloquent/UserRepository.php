<?php

namespace App\Repositories\UserEloquent;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Repositories\UserEloquent\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @param @name
     * @return boolean
     */
    public function hasPermission($name)
    {
        return Cache::rememberForever('user.permission', function () use ($name){
            $permission = DB::table('users')
                ->leftJoin('roles','roles.id','users.role_id')
                ->leftJoin('role_permissions','role_permissions.role_id','roles.id')
                ->leftJoin('permissions','permissions.id','role_permissions.permission_id')
                ->where([['permissions.path',$name],['users.id',Auth::id()]]);
            return $permission->exists();
        });
    }
}