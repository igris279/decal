<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\UserEloquent\UserRepositoryInterface;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = app()->make(UserRepositoryInterface::class);
        
        if($user->hasPermission($request->route()->getName()))
        {
            return $next($request);
        }
        return back()->with([
            'status' => 'error',
            'message' => 'Bạn không có quyền thực hiện chức năng này'
        ]);
    }
}
