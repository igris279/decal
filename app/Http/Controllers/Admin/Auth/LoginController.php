<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except(['logout']);
    }

    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    public function login(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if($validate->fails())
        {
            return back()->withInput()->withErrors($validate->errors());
        }
        else
        {
            $credentials = $request->only(['email', 'password']);
            if(Auth::attempt($credentials, $request->remember))
            {
                return redirect()->route('admin.home');
            }
            else
            {
                return back()->withErrors('Tài khoản hoặc mật khẩu không chính xác');
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.auth.form');
    }
}
